import * as fs from "node:fs/promises";
import path from "path";

const safe_directory_name = "Blog";
const target_path = "target";
const pages_path = "pages";

async function main() {
 // simple safety switch
 if (path.basename(process.cwd()) != safe_directory_name) throw new Error(
  "Working directory is not " + safe_directory_name + "." + " " +
  "You may need to change your current working directory." + " " +
  "You may also change the safe_directory_name variable at the top of build.js"
 );
 // create target or load target directory
 let target;
 try {
  target = await fs.opendir(target_path);
 }
 catch(error) {
  await fs.mkdir(target_path);
  target = await fs.opendir(target_path);
 }
 // clear target directory
 for await (const dirent of target) {
  fs.rm(
   target.path + path.sep + dirent.name,
   {recursive: true}
  );
 }
 // create a list of all the complete file paths in pages
 const pages = await fs.opendir(pages_path);
 let pages_files, pages_directories, pages_links;
 [pages_files, pages_directories, pages_links] = await inventory(pages);
 console.log(pages_files);
 console.log(pages_directories);
 console.log(pages_links);
}
main();

async function inventory(
 dir, rel_path = dir.path,
 dir_files = [], dir_directories = [], dir_links = []
) {
 for await (const dirent of dir) {
  const child_path = rel_path + path.sep + dirent.name;
  if (dirent.isFile()) {
   dir_files.push(child_path);
  }
  else if (dirent.isDirectory()) {
   dir_directories.push(child_path);
   const child_dir = await fs.opendir(child_path);
   await inventory(
    child_dir, child_path,
    dir_files, dir_directories, dir_links
   );
  }
  else if (dirent.isSymbolicLink()) {
   dir_links.push(child_path);
  }
  else throw new Error(dirent.name + " is not a file, directory, or symbolic link");
 }
 return [dir_files, dir_directories, dir_links];
}
// Documents/SimpleStaticSiteGenerator
/*
 const link_url = await fs.readlink(sub_url);
 const target_url = path.dirname(sub_url) + path.sep + link_url;
 const rel_target_url = path.relative(top, target_url);
 if (rel_target_url.includes("..")) {
  throw new Error("relative links must be contained in source directory");
 }
 const rel_sub_url = path.relative(top, sub_url);
 link_paths.push({set_path: rel_sub_url, get_path: rel_target_url});
*/
